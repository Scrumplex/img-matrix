img-matrix
----------
Generate a image matrix in html.

# Usage Example
```bash
img-matrix -i logo.png -x 200 -y 200 -o index.html
```
